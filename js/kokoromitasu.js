$(function(){
  $('a[href^="#"]').click(function(){
    let speed = 500;
    let href= $(this).attr("href");
    let target = $(href == "#" || href == "" ? 'html' : href);
    let position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});

// スクロールフェードイン
$(function () {
  $(window).scroll(function () {
    $(".fadeIn").each(function () {
      var elemPos = $(this).offset().top; /* 要素の位置を取得 */
      var scroll = $(window).scrollTop(); /* スクロール位置を取得 */
      var windowHeight = $(window).height(); /* 画面幅を取得（画面の下側に入ったときに動作させるため） */
      if (scroll > elemPos - windowHeight + windowHeight/5){
        /* 要素位置までスクロール出来たときに動作する */
        $(this).addClass("fadeNow");
      }
    });
  });
  jQuery(window).scroll();
});